import React from "react";
// Components
import TrafficImages from "./TrafficImages";
// Libraries
import {
  render,
  cleanup,
  fireEvent,
  act,
  waitFor,
} from "@testing-library/react";

describe("Pages/Main: <TrafficImages />", () => {
  const props = {
    data: [
      {
        camera_id: "1",
        url: "http://localhost/someurl1",
        ts: "1",
        coordinates: { latitude: 1, longitude: 1 },
      },
      {
        camera_id: "2",
        url: "http://localhost/someurl2",
        ts: "2",
        coordinates: { latitude: 2, longitude: 2 },
      },
    ],
  };

  beforeEach(() => {});

  afterEach(() => {
    cleanup();
  });

  it("should render without errors", () => {
    // Given:

    // When:
    const { container, getByText } = render(<TrafficImages {...props} />);

    // Then:
    expect(getByText("Traffic Images (2)")).toBeInTheDocument();
    const trafficImageEl = container.querySelectorAll(".traffic-image");
    expect(trafficImageEl).toHaveLength(2);
    props.data.forEach((it, i) => {
      // Image
      const imgEl = trafficImageEl[i].querySelector("img");
      expect(imgEl.src).toEqual(it.url);
      expect(imgEl.alt).toEqual(it.camera_id);

      // Camera ID
      expect(getByText(it.camera_id)).toBeInTheDocument();

      // Lat long
      expect(
        getByText(`(${it.coordinates.latitude}, ${it.coordinates.longitude})`)
      ).toBeInTheDocument();

      // Timestamp
      expect(getByText(it.ts)).toBeInTheDocument();
    });
  });
});
