import React from "react";

// Libraries
import DateFnsUtils from "@date-io/moment";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
// Components
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import {
  Grid,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core";
// Styles
const useStyles = makeStyles({
  formControl: {
    display: "block",
    marginBottom: "1em",
  },
});

const Filter = ({ values, data, handleDateChange, handleLocationChange }) => {
  const classes = useStyles();

  return (
    <Grid id="main-filter" container spacing={2}>
      {/* Date input */}
      <Grid item lg={6} md={6} sm={12} xs={12}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <DateTimePicker
            className={classes.formControl}
            fullWidth
            disableFuture
            id="date-picker"
            label="Date/Time"
            value={values.date}
            onChange={handleDateChange}
          />
        </MuiPickersUtilsProvider>
      </Grid>

      {/* Locations input */}
      <Grid item lg={6} md={6} sm={12} xs={12}>
        <FormControl className={classes.formControl}>
          <InputLabel shrink id="locations-select-label">
            Locations ({data.locations.length})
          </InputLabel>
          <Select
            labelId="locations-label"
            fullWidth
            id="locations-select"
            value={values.location}
            onChange={handleLocationChange}
          >
            <MenuItem key={0} value={0}>
              Select
            </MenuItem>
            {data.locations.map((it) => {
              return (
                <MenuItem key={it.id} value={it.id}>
                  {it.name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  );
};

Filter.propTypes = {
  values: PropTypes.shape({
    location: PropTypes.number,
    date: PropTypes.object,
  }),
  data: PropTypes.shape({
    locations: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
      })
    ),
  }),
  handleDateChange: PropTypes.func.isRequired,
  handleLocationChange: PropTypes.func.isRequired,
};

export default Filter;
