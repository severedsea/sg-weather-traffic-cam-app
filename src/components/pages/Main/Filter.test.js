import React from "react";
// Components
import Filter from "./Filter";
// Libraries
import moment from "moment-timezone";
import {
  render,
  cleanup,
  fireEvent,
  act,
  waitFor,
} from "@testing-library/react";

describe("Pages/Main: <Filter />", () => {
  const props = {
    values: { location: 0, date: moment("2020-01-01T00:00:00+08:00") },
    data: { locations: [{ id: 1, name: "first" }] },
    handleDateChange: jest.fn(),
    handleLocationChange: jest.fn(),
  };

  beforeEach(() => {
    props.handleDateChange.mockReset();
    props.handleLocationChange.mockReset();
  });

  afterEach(() => {
    cleanup();
  });

  it("should render without errors", () => {
    // Given:

    // When:
    const { container } = render(<Filter {...props} />);

    // Then:
    const filterContainer = container.querySelector("#main-filter");
    expect(filterContainer).toBeInTheDocument();

    // Then: Date picker should be present
    const datePickerEl = container.querySelector("#date-picker");
    expect(datePickerEl).toBeInTheDocument();

    // Then: Select input is present
    const selectEl = container.querySelector("#locations-select");
    expect(selectEl).toBeInTheDocument();
  });

  it("should trigger handleDateChange", () => {
    // Given:

    // When: Rendered
    const { container, debug } = render(<Filter {...props} />);

    // Then: Date picker should be present
    const datePickerEl = container.querySelector("#date-picker");
    expect(datePickerEl).toBeInTheDocument();

    // When: Click on date picker input
    act(() => {
      fireEvent.click(datePickerEl);
    });

    // Then: Modal dialog should be present
    const modalDialogEl = document.body.querySelector(
      ".MuiPickersModal-dialogRoot"
    );
    expect(modalDialogEl).toBeInTheDocument();

    // Then: 1st week Thu day button should be present
    const thuDayButtonEl = modalDialogEl.querySelector(
      ".MuiPickersCalendar-week:nth-child(1) > div:nth-child(5) button"
    );
    expect(thuDayButtonEl).toBeInTheDocument();

    // When: Click on date picker (1st week Thu) input
    act(() => {
      fireEvent.click(thuDayButtonEl);
    });

    // When: Click on OK button
    const okButtonEl = modalDialogEl.querySelector(
      ".MuiDialogActions-root button:nth-child(2)"
    );
    expect(okButtonEl).toBeInTheDocument();
    act(() => {
      fireEvent.click(okButtonEl);
    });

    // Then: handleDateChange triggered
    expect(props.handleDateChange).toBeCalledTimes(1);
    expect(props.handleDateChange.mock.calls[0][0].format("MMDDYYYY")).toBe(
      "01022020"
    );
  });
});
