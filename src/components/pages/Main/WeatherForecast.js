import React from "react";

// Libraries
import PropTypes from "prop-types";

const WeatherForecast = ({ data }) => {
  return (
    <>
      {data?.weather2h && (
        <div>
          <h3>2-Hour Weather Forecast</h3>
          {data.weather2h}
        </div>
      )}
    </>
  );
};

WeatherForecast.propTypes = {
  data: PropTypes.shape({
    weather2h: PropTypes.string,
  }),
};

export default WeatherForecast;
