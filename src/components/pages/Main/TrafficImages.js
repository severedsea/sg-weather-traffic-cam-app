import React, { useState, useEffect } from "react";

// Libraries
import PropTypes from "prop-types";

const TrafficImages = ({ data }) => {
  const [imagesLoaded, setImagesLoaded] = useState(0);

  useEffect(() => {
    setImagesLoaded(0);
  }, [data]);

  /**
   * Image on load handler
   */
  const _imageLoaded = () => {
    setImagesLoaded((prevState) => {
      return prevState + 1;
    });
  };

  return (
    <div className="traffic-images-container">
      <h3>Traffic Images ({data.length})</h3>
      <div
        style={{
          display: imagesLoaded !== data.length ? "none" : "block",
        }}
      >
        {data?.map((it) => {
          return (
            <div key={it.camera_id} className="traffic-image">
              <img alt={it.camera_id} src={it.url} onLoad={_imageLoaded}></img>
              <div className="traffic-image_info">
                <div>
                  ({it.coordinates.latitude}, {it.coordinates.longitude})
                </div>
                <div>Camera: {it.camera_id}</div>
              </div>
              <div className="traffic-image_ts">{it.ts}</div>
            </div>
          );
        })}
      </div>
      {data.length > 0 && imagesLoaded < data.length && "Loading..."}
    </div>
  );
};

TrafficImages.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      camera_id: PropTypes.string,
      url: PropTypes.string,
      ts: PropTypes.string,
      coordinates: PropTypes.shape({
        latitude: PropTypes.number,
        longitude: PropTypes.number,
      }),
    })
  ),
};

export default TrafficImages;
