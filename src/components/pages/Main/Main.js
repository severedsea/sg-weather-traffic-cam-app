import React, { PureComponent } from "react";

// Libraries
import moment from "moment-timezone";
// Components
import { Container } from "@material-ui/core";
import WeatherForecast from "./WeatherForecast";
import TrafficImages from "./TrafficImages";
import Filter from "./Filter";
// Stubs
import GovStubs from "../../../stubs/gov.js";
// Helpers
import Utils from "./utils.js";
// Styles
import "./index.scss";

class Main extends PureComponent {
  state = {
    selected: {
      date: moment(),
      location: 0,
      location_details: null,
      images: [],
    },
    data: {
      locations: [],
      images: [],
    },
  };

  componentDidMount() {
    this._refreshData(this.state.selected.date?.toDate());
  }

  /**
   * Refreshes all the data by selected date
   * @param {Date} date Date object
   */
  _refreshData = (date) => {
    Promise.all([
      this._refreshWeatherForecast(date),
      this._refreshTrafficImages(date),
    ]).then((data) => {
      const [locations, images] = data;
      // Update nearest location for each traffic cam image
      this._updateImageNearestLocation(locations, images);
      // Refresh selected location details
      this._updateSelectedLocation(this.state.selected.location, locations);
      // Set locations into state
      this.setState((prevState) => {
        return {
          ...prevState,
          data: {
            ...prevState.data,
            locations: locations,
          },
        };
      });
    });
  };

  /**
   * Refreshes weather forecast and locations information based on the selected date/time
   * @param {Date} date Date object
   */
  _refreshWeatherForecast = (date) => {
    return GovStubs.get2hWeatherForecast({
      date_time: date,
    })
      .then((res) => {
        const { data } = res;
        if (!data) {
          console.error("Missing data", res);
          return;
        }

        let locations = data.area_metadata.map((it, id) => {
          let weather2h;
          if (data.items && data.items.length > 0) {
            weather2h = data.items[0].forecasts.find((f) => {
              return f.area === it.name;
            })?.forecast;
          }

          return {
            id: id + 1,
            name: it.name,
            coordinates: it.label_location,
            weather2h: weather2h,
          };
        });
        // Sort ascending
        locations.sort((a, b) => {
          if (a.name > b.name) {
            return 1;
          } else if (a.name === b.name) {
            return 0;
          }
          return -1;
        });
        return Promise.resolve(locations ? locations : []);
      })
      .catch((error) => {
        console.error(error);
        return Promise.reject(error);
      });
  };

  /**
   * Refreshes list of traffic cam images based on the selected date/time
   * @param {Date} date Date object
   */
  _refreshTrafficImages(date) {
    return GovStubs.getTrafficImages({
      date_time: date,
    })
      .then((res) => {
        const { data } = res;
        if (!data) {
          console.error("Missing data", res);
          return;
        }

        if (data.items && data.items.length > 0 && data.items[0].cameras) {
          const images = data.items[0].cameras?.map((it) => {
            return {
              ts: it.timestamp,
              camera_id: it.camera_id,
              url: it.image,
              coordinates: it.location,
            };
          });
          return Promise.resolve(images ? images : []);
        }
        return Promise.resolve([]);
      })
      .catch((error) => {
        console.error(error);
        return Promise.reject(error);
      });
  }

  /**
   * Date change handler
   * @param {Object} selected Moment date object selected
   */
  _handleDateChange = (selected) => {
    this.setState((prevState) => {
      return {
        ...prevState,
        selected: {
          ...prevState.selected,
          date: selected,
        },
      };
    });

    this._refreshData(selected?.toDate());
  };

  /**
   * Updates state.data.images with the nearest location from state.data.locations based on distance
   * @param {[]Object} locations Locations with weather forecast information
   * @param {[]Object} images Traffic images
   */
  _updateImageNearestLocation = (locations, images) => {
    if (!locations || !images) {
      return [];
    }

    // Iterate over each image
    const result = images.map((it) => {
      // Find location nearest to the traffic cam image
      let min = {
        location_id: 0,
        distance: Number.MAX_SAFE_INTEGER,
      };
      locations.forEach((l) => {
        const distance = Utils.calculateDistance(
          it.coordinates.latitude,
          it.coordinates.longitude,
          l.coordinates.latitude,
          l.coordinates.longitude
        );
        if (min.distance > distance) {
          min.id = l.id;
          min.distance = distance;
        }
      });

      return {
        ...it,
        nearest_location_id: min.id ? min.id : null,
      };
    });

    this.setState((prevState) => {
      return {
        ...prevState,
        data: {
          ...prevState.data,
          images: result,
        },
      };
    });
  };

  /**
   * Location change handler
   * @param {Event} e
   */
  _handleLocationChange = (e) => {
    const selected = e.target.value;
    this._updateSelectedLocation(selected, this.state.data.locations);
  };

  /**
   * Updates the selected location details
   * @param {number} selected Selected location ID
   * @param {[]Object} locations Available locations
   */
  _updateSelectedLocation = (selected, locations) => {
    const location = locations.find((it) => {
      return it.id === selected;
    });

    this.setState((prevState) => {
      return {
        ...prevState,
        selected: {
          ...prevState.selected,
          location: location ? selected : 0,
          location_details: location,
          images: this.state.data.images.filter((it) => {
            return it.nearest_location_id === selected;
          }),
        },
      };
    });
  };

  render() {
    return (
      <div className="main">
        <Container maxWidth="lg">
          <h1>Singapore Weather Forecast and Traffic Cam</h1>
          {/* Query filter */}
          <Filter
            values={this.state.selected}
            data={this.state.data}
            handleDateChange={this._handleDateChange}
            handleLocationChange={this._handleLocationChange}
          />
          {/* Weather Forecast */}
          <WeatherForecast data={this.state.selected.location_details} />
          {/* Traffic Images */}
          <TrafficImages data={this.state.selected.images} />
        </Container>
      </div>
    );
  }
}

export default Main;
