import React from "react";
// Components
import Main from "./Main";
// Libraries
import {
  render,
  cleanup,
  fireEvent,
  act,
  waitFor,
} from "@testing-library/react";

describe("Pages/Main: <Main />", () => {
  beforeEach(() => {});

  afterEach(() => {
    cleanup();
  });

  it("should render without errors", () => {
    // Given:

    // When:
    const { container, getByText } = render(<Main />);

    // Then:
    const mainEl = container.querySelector(".main");
    expect(mainEl).toBeInTheDocument();

    // Then: Filter component rendered
    const filterContainer = container.querySelector("#main-filter");
    expect(filterContainer).toBeInTheDocument();

    // Then: Traffic images rendered
    expect(getByText("Traffic Images (0)")).toBeInTheDocument();
  });

  // TODO: Add more tests on interactions
});
