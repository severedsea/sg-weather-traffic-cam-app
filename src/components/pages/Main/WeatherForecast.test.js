import React from "react";
// Components
import WeatherForecast from "./WeatherForecast";
// Libraries
import {
  render,
  cleanup,
  fireEvent,
  act,
  waitFor,
} from "@testing-library/react";

describe("Pages/Main: <WeatherForecast />", () => {
  const props = {
    data: { weather2h: "Sunny" },
  };

  beforeEach(() => {});

  afterEach(() => {
    cleanup();
  });

  it("should render without errors", () => {
    // Given:

    // When:
    const { getByText } = render(<WeatherForecast {...props} />);

    // Then:
    expect(getByText("2-Hour Weather Forecast")).toBeInTheDocument();
    expect(getByText(props.data.weather2h)).toBeInTheDocument();
  });
});
