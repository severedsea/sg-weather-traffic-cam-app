// Libraries
import axios from "axios";
import moment from "moment-timezone";

/**
 * Get 2-hour weather forecast
 * https://data.gov.sg/dataset/weather-forecast?resource_id=571ef5fb-ed31-48b2-85c9-61677de42ca9
 * @param {Object} payload Query arguments
 * @param {Date} [payload.date_time] Retrieves the latest forecast issued at that moment in time
 */
const get2hWeatherForecast = (payload) => {
  const params = {};

  if (payload.date_time) {
    params["date_time"] = _toRFC3339SGTString(payload.date_time);
  }

  return new Promise((resolve, reject) => {
    _ajaxRequest({
      method: "GET",
      url: `https://api.data.gov.sg/v1/environment/2-hour-weather-forecast`,
      params: params,
    })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err.errors || err);
      });
  });
};

/**
 * Get traffic images
 * https://data.gov.sg/dataset/traffic-images
 * @param {Object} payload Query arguments
 * @param {Date} [payload.date_time] Retrieve the latest available data at that moment in time
 */
const getTrafficImages = (payload) => {
  const params = {};

  if (payload.date_time) {
    params["date_time"] = _toRFC3339SGTString(payload.date_time);
  }

  return new Promise((resolve, reject) => {
    _ajaxRequest({
      method: "GET",
      url: `https://api.data.gov.sg/v1/transport/traffic-images`,
      params: params,
    })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err.errors || err);
      });
  });
};

/**
 * Formats date into RFC3339 (SGT) string
 * @param {Date} date Date object
 * @param {string} [format] Optional date format layout
 */
const _toRFC3339SGTString = (date, format = "YYYY-MM-DD[T]HH:mm:ss") => {
  return moment(date).tz("Asia/Singapore").format(format);
};

/**
 * Executes an ajax request made to data.gov.sg
 * @param {Object} config axios config
 */
const _ajaxRequest = async (config) => {
  const options = {
    ...config,
    headers: {
      ...config.headers,
      "Content-Type": "application/json",
    },
  };

  return new Promise((resolve, reject) => {
    axios({
      ...options,
    })
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        const errObj = {};
        if (err.response) {
          errObj.errors = [
            {
              error: err.response.data,
              status: err.response.status,
              message: err.response.data?.message
                ? err.response.data?.message
                : "An error has occurred. Please try again later.",
            },
          ];
        }
        reject(errObj);
      });
  });
};

export default {
  get2hWeatherForecast,
  getTrafficImages,
};
