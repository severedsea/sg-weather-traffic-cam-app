import GovStubs from "./gov.js";
// Libraries
import axios from "axios";

jest.mock("axios");

describe.each([
  [
    "no date_time",
    {},
    {
      method: "GET",
      url: `https://api.data.gov.sg/v1/environment/2-hour-weather-forecast`,
      params: {},
      headers: {
        "Content-Type": "application/json",
      },
    },
  ],
  [
    "should convert date_time to RFC3339 SGT string",
    {
      date_time: new Date("2020-01-02T00:00:00+00:00"),
    },
    {
      method: "GET",
      url: `https://api.data.gov.sg/v1/environment/2-hour-weather-forecast`,
      params: {
        date_time: "2020-01-02T08:00:00",
      },
      headers: {
        "Content-Type": "application/json",
      },
    },
  ],
])("(GovStubs) get2hWeatherForecast", (desc, given, expected) => {
  test(desc, async () => {
    // Given:
    const mockResponse = {
      status: 200,
    };

    let actual;
    axios.mockImplementation((options) => {
      actual = options;
      return Promise.resolve(mockResponse);
    });

    // When:
    const request = GovStubs.get2hWeatherForecast(given);

    // Then:
    await expect(request).resolves.toEqual(mockResponse);
    expect(actual).toMatchObject(expected);
  });
});

describe.each([
  [
    "no date_time",
    {},
    {
      method: "GET",
      url: `https://api.data.gov.sg/v1/transport/traffic-images`,
      params: {},
      headers: {
        "Content-Type": "application/json",
      },
    },
  ],
  [
    "should convert date_time to RFC3339 SGT string",
    {
      date_time: new Date("2020-01-02T00:00:00+00:00"),
    },
    {
      method: "GET",
      url: `https://api.data.gov.sg/v1/transport/traffic-images`,
      params: {
        date_time: "2020-01-02T08:00:00",
      },
      headers: {
        "Content-Type": "application/json",
      },
    },
  ],
])("(GovStubs) getTrafficImages", (desc, given, expected) => {
  test(desc, async () => {
    // Given:
    const mockResponse = {
      status: 200,
    };

    let actual;
    axios.mockImplementation((options) => {
      actual = options;
      return Promise.resolve(mockResponse);
    });

    // When:
    const request = GovStubs.getTrafficImages(given);

    // Then:
    await expect(request).resolves.toEqual(mockResponse);
    expect(actual).toMatchObject(expected);
  });
});
