import React from "react";
import { MainPage } from "./components/pages/Main";

function App() {
  return (
    <>
      <main className="app-main">
        <MainPage />
      </main>
    </>
  );
}

export default App;
