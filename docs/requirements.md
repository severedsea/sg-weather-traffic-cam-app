# Requirements

The team wants to create a simple application utilizing the open source data from
https://data.gov.sg/developer#realtime. You will be using the following APIs:

1. Traffic Images (https://data.gov.sg/dataset/traffic-images)
1. Weather Forecast (https://data.gov.sg/dataset/weather-forecast)

## Features

1. Allow the user to choose a date and time, then show a list of locations with traffic cam photos for the specified date and time. (API 1: Traffic Images)
1. Show the list of locations from API 1 (Traffic Images) only has lat/long without name, use a reverse geocoding service (API 2: Weather Forecast) to display more user friendly location names
1. When the user selects a location from the list, show the traffic cam photo, and also the weather info for that location from API 2 (Weather Forecast) (or the nearest available weather info depending on what API 2 can return)

As the location information are returned in latitude and longitude format, you are required to look for an API that does reverse geo-coding to show its human readable names instead.

## What are you supposed to do?

1. Create a frontend application based on the requirements and mock-up above
1. Create proper UI components for the application
1. Handle the responsiveness of the UI in the application
1. Create the necessary CSS styling ensure the interface is aesthetically pleasing
1. Develop an application with good user-experience (UX)
1. Feel free to change the design of the application if you have a better idea
1. You will be asked to showcase and explain to us some of your development concept on the interview day itself.
