# Design Decisions and Assumptions

- Instead of letting the user select a traffic cam for the selected date, we'll let the user select a location, where it will show both the weather forecast as well as the traffic cam images that are near that area. I find it more intuitive to present the traffic cam images this way since area/location is more relevant to the user than selecting a particular camera.
- Didn't use redux/reducer pattern since the application seems to be small enough.
- Built components but not design to be shared since there is no use-case at this point. When that time comes, we'll need to refactor those components to make it more generic and fit the requirements.
