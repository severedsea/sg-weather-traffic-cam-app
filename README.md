# SG Weather Forecast and Traffic Cam App

Web application that allows users to view traffic cam snapshot and weather conditions/forecast based on a selected date and location in Singapore

## Documentation

- [Requirements](docs/requirements.md)
- [Screenshots](docs/screenshots.md)
- [Design](docs/design.md)

## Setup

### Prerequisites

- [Node](https://nodejs.org/en/download/) v12+
- [Yarn](https://yarnpkg.com/getting-started/install) 1.22.4+

### Running Locally

Environment variables can be configured in `.env`

```
yarn start
```

## Development

## Test

Executs all the tests. Test environment variables can be configured in `.env`

```
yarn test
```
